<?php
/**
 * Główny plik Vatrena-Cropper
 * 
 * Plik odpowiedzialny za działanie aplikacji
 * 
 * @author Wojciech Brzeziński <w.brzezinski@vatrena-cms.pl>
 * @package  Vatrena-Cropper
 * @subpackage Vatrena-Cropper-Cropp
 * @version 1.0
 * @since 14-11-20414 19:31
 */

/**
 * @since 14-11-2014 19:33 Version 1.0
 * @package  Vatrena-Cropper
 * @subpackage Vatrena-Cropper-Cropp
 */
class Cropper{
    /**
     * Objekt przechowujący dane
     * 
     * Objekt odpowiedzialny za przechowanie tablicy z danymi
     * 
     * @var array
     * @since 14-11-2014 19:33 Version 1.0
     * @access private
     * @static
     */
    private static $data;
    
    /**
     * Objekt przechowujący obrazek
     * 
     * Objekt przechowujący instancję biblioteki Image Magician w której otwarty jest obrazek źródłowy
     * 
     * @var object
     * @access private
     * @since 14-11-2014 20:00 Version 1.0
     * @static
     */
    private static $obrazek;

    /**
     * Konstruktor klasy
     * 
     * Konstruktor klasy odpowiedzialny za ustawienie odpowiedniej wartości objektów
     * 
     * @access public
     * @return void
     * @since 14-11-2014 19:34 Version 1.0
     * @param array $data Tablica z danymi
     */
    function __construct($data) {
        self::$data = $data;
    }
    
    /**
     * Metoda ładująca obrazek
     * 
     * Metoda odpowiedzialna za załadowanie obrazka
     * 
     * @access public
     * @return void
     * @since 14-11-2014 19:59 Version 1.0
     */
    function laduj_obrazek(){
        self::$obrazek = new imageLib('temp/in.jpg');
    }
    
    /**
     * Metoda przycinająca
     * 
     * Metoda odpowiedzialna za przycięcie obrazka do podanych wielkości
     * 
     * @since 14-11-2014 20:08 Version 1.0
     * @access public
     * @return void
     */
    function przycinanie(){
        self::$obrazek->cropImage(self::$data['height'], self::$data['width'], self::$data['x'].'x'.self::$data['y']);
    }
    
    /**
     * Metoda zapisująca
     * 
     * Metoda odpowiedzialna za zapis obrazka do pliku *.png
     * 
     * @access public
     * @return void
     * @since 14-11-2014 20:11 Version 1.0
     */
    function zapis(){
        self::$obrazek->saveImage('out/out.jpg', 100);
    }
}

if(isset($_POST['data'])){
    require_once './imagemagican/php_image_magician.php';
    $cropper = new Cropper($_POST['data']);
    $cropper->laduj_obrazek();
    $cropper->przycinanie();
    $cropper->zapis();
}