<?php
/**
 * Główny plik Vatrena-Cropper
 * 
 * Plik odpowiedzialny za działanie aplikacji
 * 
 * @author Wojciech Brzeziński <w.brzezinski@vatrena-cms.pl>
 * @package  Vatrena-Cropper
 * @subpackage Vatrena-Cropper-Saver
 * @version 1.0
 * @since 14-11-20414 19:31
 */

/**
 * @package  Vatrena-Cropper
 * @subpackage Vatrena-Cropper-Saver
 * @since 14-11-2014 20:23 Version 1.0
 */
class Saver{
    /**
     * Objekt przechowujący dane o pliku
     * 
     * Objekt przechowujący tablicę z danymi o przesłanym pliku
     * 
     * @var array
     * @since 14-11-2014 20:27 Version 1.0
     * @access private
     * @static
     */
    private static $data;
    
    /**
     * Objekt przchowujący ścieżkę
     * 
     * Objekt przechowujący ścieżkę w której zapisany ma zostać obrazek
     * 
     * @var string
     * @access private
     * @static
     * @since 14-11-2014 20:34 Version 1.0
     */
    private static $path = 'temp/in.jpg';
    
    /**
     * Konstruktor klasy
     * 
     * Konstruktor nadający wartości objektom
     * 
     * @since 14-11-2014 20:28 Version 1.0
     * @param array $data Dane o pliku
     * @access public
     * @return void
     */
    function __construct($data) {
        self::$data = $data;
    }
    
    /**
     * Metoda zapisująca obrazek
     * 
     * Metoda odpowiedzialna za końcowy zapis pliku
     * 
     * @since 14-11-2014 20:31 version 1.0
     * @access public
     * @return void
     */
    function f_saver(){
        move_uploaded_file(self::$data['tmp_name'], self::$path);
    }
}

if(isset($_FILES['obrazek'])){
    $saver = new Saver($_FILES['obrazek']);
    $saver->f_saver();
    header("Refresh:0.5; url=./");
}