/****************************************************/
/*          Created by Wojciech Brzeziński          */
/*               Copyright Vatrena-CMS              */
/****************************************************/


+(function ($, Document, window, undefined) {
    "use strict";
    
    var crop_data;
    
    $(document).ready(function(){
        $(".container > img").cropper({
            data: {
                width: 450,
                height: 380
            },
            resizable: false,
            zoomable: false,
            rotatable: false,
            done: function(data){
                crop_data = data;
            }
        });
    });
    
    var $btn_handler = '#set_data';
    
    $(document).on('click', $btn_handler, function(){
        console.log(crop_data);
        
        $.ajax({
            type: "POST",
            url: "cropper.php",
            data: {data: crop_data}
        }).done(function(){
            alert('Gotowe');
        });
        
        return false;
    });
    
    var $sender_handler = "#send";
    
})(jQuery, Document, window, undefined);